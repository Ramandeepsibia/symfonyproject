<?php 

// src/AppBundle/Controller/LuckyController.php
namespace AppBundle\Controller;

use AppBundle\Entity\Genus;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;


class GenusController extends Controller
{
    /**
     * @Route("/genus/new")
     */
    public function newAction()
    {
        $number = mt_rand(0, 100);

        $genus = new Genus();
        $genus->setName('Octopus'.$number);
        
        
        $genus->setSubFamily('Octopodinae');
        $genus->setSpeciesCount(rand(100, 99999));
        $genus->setFunFact("very Funny");

        $em = $this->getDoctrine()->getManager();
        $em->persist($genus);
        $em->flush();

        /**return $this->render('lucky/number.html.twig', array(
            'number' => $number,
        ));**/
        return new Response('<html><body>Genus created!</body></html>');
    }
    /**
     * @Route("/genus/{genusName}")
     */
    public function showAction($genusName)
    {

    }
    /**
     * @Route("/genus")
     */
    public function listAction()
    {
        $em = $this->getDoctrine()->getManager();
        $genuses = $em->getRepository('AppBundle\Entity\Genus')
            ->findAll();

        return $this->render('lucky/number.html.twig', [
            'genuses' =>  $genuses
        ]);
        dump($genuses);die;
    }

}